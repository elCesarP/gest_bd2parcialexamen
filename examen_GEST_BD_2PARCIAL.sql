create table POLICIA (id_POLICIA VARCHAR (10) not null primary key,
nombres varchar(20), apellidos varchar(20), nintentos_perdidos integer);

create table CAMPO (id_campo varchar (10) not null primary key, 
nombre_campo varchar (20), naprobados integer, nreprobados integer);

create table INGRESO (id_INGRESO varchar (10) not null primary key,
id_POLICIA VARCHAR (10)references POLICIA (id_POLICIA),
id_campo varchar (10)references CAMPO (id_campo),
AÑO integer, estado varchar (20));


alter table POLICIA ADD CHECK (nintentos_perdidos<3);

insert into POLICIA (id_POLICIA,nombres,apellidos,nintentos_perdidos) values
('1','ana','bella',1);


insert into CAMPO (id_campo,nombre_campo,naprobados,nreprobados) values 
('5','las aguilas',0,1 );



CREATE PROCEDURE insertar_INTENTO (
    IN p_id_INGRESO VARCHAR(10),
    IN p_id_POLICIA VARCHAR(10),
    IN p_id_campo VARCHAR(10),
    IN P_AÑO INTEGER,
    IN P_estado VARCHAR(20)
)
LANGUAGE plpgsql
AS $$
BEGIN
    INSERT INTO INGRESO (id_INGRESO,id_POLICIA,id_campo,AÑO,estado)
    VALUES (p_id_INGRESO, p_id_POLICIA, p_id_campo,P_AÑO, p_estado);

    IF p_estado = 'reprobado' THEN
        UPDATE POLICIA
        SET nintentos_perdidos = nintentos_perdidos + 1
        WHERE id_POLICIA = p_id_POLICIA;

        UPDATE CAMPO
        SET nreprobados = nreprobados + 1
        WHERE id_campo = p_id_campo;
    ELSIF p_estado = 'aprobado' THEN
        UPDATE CAMPO
        SET naprobados = naprobados + 1
        WHERE id_campo = p_id_campo;
    END IF;

EXCEPTION
    WHEN OTHERS THEN 
        ROLLBACK;
        RAISE EXCEPTION 'No se pudo ejecutar la transacción';
END;
$$;
select * from POLICIA;
select * from CAMPO;
select * from INGRESO;
call insertar_INTENTO ('31','1','5',2006,'reprobado');